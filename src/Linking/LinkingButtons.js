import React from "react";
import { Linking, Platform } from 'react-native';
import * as SMS from 'expo-sms';

// export default class LinkingButtons extends React.Component {

    export const onSmsPress = async () => {
        const isAvailable = await SMS.isAvailableAsync();
        if (isAvailable) {
          SMS.sendSMSAsync('+923488918138', 'Hello');
          console.log("Open")
        } else {
          // misfortune... there's no SMS available on this device
        }
      }
    
      export const onDialCallPress = () => {
    
        let phoneNumber = '';
    
        if (Platform.OS === 'android') {
          phoneNumber = 'tel:${+923488918138}';
        }
        else {
          phoneNumber = 'telprompt:${+923488918138}';
        }
    
        Linking.openURL(phoneNumber);
      };
    
      export const onWhatsappPress = () => {
        Linking.openURL('http://api.whatsapp.com/send?phone=+92'+'3488918138')
      }