import { StatusBar } from 'expo-status-bar';
import React, { Component, useEffect } from 'react';
import { StyleSheet, Text, View, SafeAreaView, TextInput, TouchableOpacity, FlatList, KeyboardAvoidingView } from 'react-native';
import { Ionicons, Feather, EvilIcons } from '@expo/vector-icons';
import * as Contacts from "expo-contacts";
import { Platform } from 'react-native';

export default class AddCustomer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      enterCode: false,
      spinner: false,
      country: {
        cca2: 'PK',
        callingCode: '92'
      },
      
    };
  }

  componentDidMount = () => {
    (async () => {
      const { status } = await Contacts.requestPermissionsAsync();
      if (status === "granted") {
        const { data } = await Contacts.getContactsAsync({
          fields: [Contacts.Fields.PhoneNumbers, Contacts.Fields.firstName],
        });

        if (data.length > 0) {
          this.setState({
            contact: data,
            // animating: false,
            // ids: [],
            // inMemoryContacts: data,
          });
          console.log(this.state.contact)
          // this.state.contact.map((item) => {
          //   this.state.dev_contacts.push(item.phoneNumbers[0].number)
          //   this.Common_Con = this.state.dev_contacts
          //   MyDeviceContacts = this.Common_Con
          //   // console.log(MyDeviceContacts)
          // })
        }
      }
    })();  
  };

    render() {
    return (
    <View style={styles.container}>
      {/* header */}
      <SafeAreaView>
        <View style={styles.headerWrap}>
            <Ionicons name="arrow-back-circle" size={30} color={'orange'} />
            <Text style={styles.headerText}>Add Customer</Text>
        </View>
      </SafeAreaView>

      {/* Search */}
      <View style={styles.searchWrap}>
        <Feather name="search" size={24} color={'grey'} />
        <View style={styles.Search}>
          <TextInput style={styles.SearchText} placeholder={"Search"} />
        </View>
      </View>

      {/* Contacts List */}
      <View style={{ flexDirection: 'row', alignSelf: 'center', backgroundColor: 'blue', width: '100%', marginTop: '2%' }}>
        <Text style={{ fontSize: 20, color: 'white', fontWeight: 'bold', marginLeft: '2%' }}> Contact List: </Text>
        <Text style={{ fontSize: 19, color: 'white'}}> Select to Add Customer</Text>
      </View>
      <View style={{ height: '45%' }}>
      <FlatList
              style={{
                alignSelf: "center",
                width: "90%",
                
              }}
              data={this.state.contact}
              // data={MyContacts}
              // keyExtractor={(item) => item.Fields}
              renderItem={(val) => (
                <View
                  style={{
                    flexDirection: "row",
                    marginBottom: 5,
                    marginTop: 5,
                    // backgroundColor: "#55b9f3",
                  }}>
                  <View>
                    <EvilIcons name="user" size={50} color="blue" />
                  </View>
                  <View style={{ width: '60%'}}>
                    <Text style={{ fontSize: 19, marginLeft: 10 }}>
                      {val.item.name ? val.item.name : null}
                    </Text>
                    <Text style={{ fontSize: 18, marginLeft: 10 }}>
                      {
                      val.item.phoneNumbers
                        ? val.item.phoneNumbers[0].number
                        : null}
                    </Text>
                  </View>
                  <View style={{ justifyContent: 'center', alignItems: 'center', alignSelf: 'center', alignContent: 'center', marginLeft: '5%'}}>
                    {/* {this.gatherContacts(val.item.phoneNumbers[0].number)} */}
                    {/* {this.CompareNum(val.item.phoneNumbers[0].number)} */}
                    {/* {this.Sign(val.item.phoneNumbers[0].number)}
                    {
                      this.check_common ? <View><FontAwesome name="registered" size={24} /></View> : null
                    } */}
                      
                  </View>
                </View>
              )}
            />
      </View>
      <KeyboardAvoidingView>
        <View style={{ alignSelf: 'center', marginTop: '5%', borderTopColor: 'orange', borderTopWidth: 2, width: '100%'}}>
            <View style={{ flexDirection: 'row', alignItems: 'center', alignSelf: 'center', marginTop: '2%' }}>
              <Feather name="user" size={24} color="black" />
              <TextInput placeholder={'Customer Name'} style={{ borderBottomColor: 'grey', borderBottomWidth: 1, fontSize: 20, width: '70%', marginLeft: '1%' }} />
            </View>
            <View style={{ flexDirection: 'row', marginTop: '3%', alignItems: 'center', alignSelf: 'center' }}>
              <Ionicons name="call-outline" size={24} color="black" />
              <TextInput placeholder={'Mobile Number (optional)'} style={{ borderBottomColor: 'grey', borderBottomWidth: 1, fontSize: 20, width: '70%', marginLeft: '1%' }} />
            </View>
        </View>
      </KeyboardAvoidingView>

      {/* Add Customer Button */}
      <View style={styles.BtnView}>
        <TouchableOpacity style={styles.BtnStyle}>
          <Text style={{ fontSize: 20, color: 'white'}}>Save</Text>
        </TouchableOpacity>
      </View>
    </View>
);
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    width: '100%',
    height: '100%',
    marginTop: '12%'
  },

  headerWrap: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingHorizontal: 20,
    alignItems: 'center',
  },

  headerText: {
    fontSize: 20,
    marginLeft: '4%',
  },

  searchWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: '2%',
    paddingHorizontal: '6%',
  },

  Search: {
    flex: 1,
    marginLeft: '3%',
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
  },

  SearchText: {
    fontSize: 18,
    marginBottom: '1%',
  },

  BtnView : {
    alignSelf: 'center',
    marginTop: '10%',
    backgroundColor: 'blue',
    width: '70%',
    height: '7%',
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },

  BtnStyle: {
    width: '100%',
    height: '100%',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
