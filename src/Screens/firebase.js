
import * as firebase from 'firebase';
import '@firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyCeFUQ7NhFp_1mrhxP-U-G6_CWJWgc4bd8",
    authDomain: "dukaandaari-42781.firebaseapp.com",
    projectId: "dukaandaari-42781",
    storageBucket: "dukaandaari-42781.appspot.com",
    messagingSenderId: "902311619286",
    appId: "1:902311619286:web:c41e93b980a5d67880f777"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
export default firebase;