import React, { Component } from 'react';
import { Text, View } from 'react-native';

import 'react-native-gesture-handler';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, HeaderTitle } from '@react-navigation/stack';

import Signup from './src/Screens/Signup.js';
import OtpVerify from './src/Screens/OtpVerifyScreen.js';
import BusinessName from "./src/Screens/businessName.js";
import AddCustomer from "./src/Screens/addCustomer.js";
import CustomerDash from './src/Screens/customerDashboard.js';
import Dashboard from './src/Screens/Dashboard.js';
import Inventory from "./src/Screens/inventory.js";
import AddInventory from "./src/Screens/addInventoryItems.js";
import EntryDetailGave from "./src/Screens/entryDetailGave.js";
import EntryDetailTook  from "./src/Screens/entryDetailTook.js";
import Receipt from './src/Screens/attachReceipt.js';

const Stack = createStackNavigator();

function MyStack() {
  return(
    <Stack.Navigator screenOptions={{
      headerShown: false
    }}>
      {/* <Stack.Screen name="OTP" component={OtpVerify} /> */}
      <Stack.Screen name="Dashboard" component={Dashboard} />
    </Stack.Navigator>
  );
}

export default class App extends Component {
  render() {
    return (
      <NavigationContainer>
        {/* <MyStack /> */}
        <Inventory />
      </NavigationContainer>
    )
  }
}
