import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, SafeAreaView, TextInput, TouchableOpacity, Alert } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

export default class BusinessName extends Component {

  constructor(props) {
    super(props);
    this.state = {
      enterCode: false,
      spinner: false,
      country: {
        cca2: 'PK',
        callingCode: '92'
      }
    };
  }

    render() {
    return (
    <View style={styles.container}>
      <View style={{ width: '100%', alignItems: 'center',}}>
        <Text style={{ fontSize: 25, fontWeight: 'bold', }}>Your Business Name</Text>
        <View>
          <Text style={{ marginTop: '2%', fontSize: 18}}>Please enter your business name </Text>
          <Text style={{ fontSize: 18}}>and start Dokaan Daari</Text>
        </View>
      </View>
      <View style={{ width: '60%' }}>
      <TextInput placeholder={'Business Name      '} style={{ fontSize: 20, marginBottom: '1%', marginTop: '5%', borderBottomColor: 'grey', borderBottomWidth: 1}} />
      </View>
      <View style={styles.BtnView}>
        <TouchableOpacity style={styles.BtnStyle}>
          <Text style={{ fontSize: 20, color: 'white'}}>Next</Text>
        </TouchableOpacity>
      </View>
      <View style={{ alignSelf: 'center', marginTop: '3%',}}>
        <TouchableOpacity>
          <Text style={{ fontSize: 20, color: 'grey'}}>Cancel</Text>
        </TouchableOpacity>
      </View>
    </View>
);
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
  },

  BtnView : {
    alignSelf: 'center',
    marginTop: '60%',
    backgroundColor: 'blue',
    width: '70%',
    height: '7%',
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },

  BtnStyle: {
    width: '100%',
    height: '100%',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
