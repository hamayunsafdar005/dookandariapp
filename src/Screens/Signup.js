import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, SafeAreaView, TextInput, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

export default function Signup() {
  return (
    <View style={styles.container}>
    <SafeAreaView style={{ width: '100%', height: '100%', marginTop: '25%'}}>
      <View style={{ alignSelf: 'flex-start', height: '15%', width: '100%', marginLeft: '5%'}}>
        <Ionicons name="arrow-back-circle-sharp" size={40} color="orange" />
      </View>
      <View style={{ marginLeft: '20%', width: '80%'}}>
        <Text style={{ fontSize: 25, fontWeight: 'bold'}}>Signup</Text>
        <Text style={{ marginTop: '2%', fontSize: 18}}>Please enter and verify your mobile</Text>
        <Text style={{ fontSize: 18}}>number in order to register your account with Dokaan Daari</Text>
      </View>
      <View style={{ alignSelf: 'center', alignItems: 'baseline', marginTop: '5%'}}>
        <TextInput placeholder={'First Name               '} style={{ fontSize: 20, marginBottom: '1%', marginTop: '1%', borderBottomColor: 'grey', borderBottomWidth: 1}} />
        {/* <TextInput placeholder={'Mobile Number      '} style={{ fontSize: 20, marginBottom: '1%', marginTop: '1%', borderBottomColor: 'grey', borderBottomWidth: 1}} /> */}
        <TextInput placeholder={'Email Address (Optional)'} style={{ fontSize: 20, marginBottom: '1%', marginTop: '1%', borderBottomColor: 'grey', borderBottomWidth: 1}} />
      </View>
      <View style={{ alignSelf: 'center', marginTop: '25%', backgroundColor: 'blue', width: '70%', height: '7%', borderRadius: 25, alignItems: 'center', justifyContent: 'center'}}>
        <TouchableOpacity style={{ width: '100%', height: '100%', alignSelf: 'center', alignItems: 'center', justifyContent: 'center'}}>
          <Text style={{ fontSize: 20, color: 'white'}}>Next</Text>
        </TouchableOpacity>
      </View>
      <View style={{ alignSelf: 'center', marginTop: '3%',}}>
        <TouchableOpacity>
          <Text style={{ fontSize: 20, color: 'grey'}}>Cancel</Text>
        </TouchableOpacity>
      </View>
      <StatusBar style="auto" />
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    height: '100%',
  },
});
