import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, SafeAreaView, TextInput, TouchableOpacity, Alert } from 'react-native';
import { Ionicons, MaterialIcons, FontAwesome, SimpleLineIcons } from '@expo/vector-icons';

export default class AddInventory extends Component {

  constructor(props) {
    super(props);
    this.state = {
      enterCode: false,
      spinner: false,
      country: {
        cca2: 'PK',
        callingCode: '92'
      }
    };
  }

    render() {
    return (
    <View style={styles.container}>
      {/* header */}
      <SafeAreaView>
        <View style={styles.headerWrap}>
            <Ionicons name="arrow-back-circle" size={37} color={'orange'} />
            <View>
              <Text style={styles.headerText}>Add Product</Text>
              <Text style={{ fontSize: 14, paddingHorizontal: 6 }}>to Inventory</Text>
            </View>
        </View>
      <View>

      <View style={{ height: '60%' }}>
        <View style={{ marginTop: '5%'}}>
          <TextInput style={styles.ItemStyle} placeholder={'Item Name'} />
          <TextInput style={styles.ItemStyle} placeholder={'Company Name'} />
          <TextInput style={styles.ItemStyle} placeholder={'Quantity in kg/ltr'} />
          <TextInput style={styles.ItemStyle} placeholder={'Price'} />
        </View>
      </View>

      <View style={{ height: '40%'}}>

        {/* Buttons */}
        <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
          <View style={styles.BtnView}>
          <TouchableOpacity style={styles.BtnStyle}>
            <Text style={{ fontSize: 20, color: 'white'}}>Save To Inventory</Text>
          </TouchableOpacity>
          </View>
        </View>
      </View>
      </View>
      </SafeAreaView>
    </View>
);
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    width: '100%',
    height: '100%',
    marginTop: '8%'
  },

  headerWrap: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingHorizontal: 20,
    alignItems: 'center',
    height: '10%',
    borderBottomColor: 'orange',
    borderBottomWidth: 1,
  },

  headerText: {
    fontSize: 20,
    marginLeft: '4%',
  },

  BtnView : {
    alignSelf: 'center',
    marginTop: '2%',
    backgroundColor: 'blue',
    width: '70%',
    height: '40%',
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },

  ItemStyle: {
    fontSize: 20,
    marginLeft: '10%',
    borderBottomColor: '#d3d3d3',
    borderBottomWidth: 1,
    width: '40%',
    marginBottom: '3%',
  },

  BtnStyle: {
    width: '100%',
    height: '100%',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
