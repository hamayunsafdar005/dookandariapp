import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, SafeAreaView, TextInput, TouchableOpacity, Alert } from 'react-native';
import { Ionicons, MaterialIcons, FontAwesome, SimpleLineIcons } from '@expo/vector-icons';

export default class EntryDetailGave extends Component {

  constructor(props) {
    super(props);
    this.state = {
      enterCode: false,
      spinner: false,
      country: {
        cca2: 'PK',
        callingCode: '92'
      }
    };
  }

    render() {
    return (
    <View style={styles.container}>
      {/* header */}
      <SafeAreaView>
        <View style={styles.headerWrap}>
            <Ionicons name="arrow-back-circle" size={37} color={'orange'} />
            <View>
              <Text style={styles.headerText}>Entry Detail</Text>
              <Text style={{ fontSize: 14, paddingHorizontal: 10 }}>for Gulraiz Iqbal</Text>
            </View>
            <MaterialIcons style={{ alignItems: 'center', paddingHorizontal: '3%', marginLeft: '15%' }} name="sms" size={24} color={'black'} />
            <Ionicons style={{ alignItems: 'center', paddingHorizontal: '3%' }} name="call" size={24} color={'black'} />
            <Ionicons style={{ alignItems: 'center', paddingHorizontal: '3%' }} name="settings" size={24} color={'black'} />
        </View>

        <View style={{ flexDirection: 'row', marginTop: '5%', alignSelf: 'flex-end', height: '5%' }}>
          <MaterialIcons style={{ alignItems: 'center', paddingHorizontal: '3%'}} name="inventory" size={30} color={'black'} />
          <Ionicons style={{ alignItems: 'center', paddingHorizontal: '3%' }} name="logo-whatsapp" size={30} color={'black'} />
          <Ionicons style={{ alignItems: 'center', paddingHorizontal: '3%' }} name="documents" size={30} color={'black'} />
        </View>
      <View>

      <View style={{ height: '45%'}}>
        <Text>Record List</Text>
      </View>

      <View style={{ height: '40%'}}>
        <View style={{ borderTopColor: 'orange', borderTopWidth: 1, marginTop: '5%', flexDirection: 'row', justifyContent: 'space-between' }}>
          <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: '5%', marginTop: '5%' }}>
            <MaterialIcons name="attach-money" size={24} color='#d3d3d3' />
            <TextInput style={{ borderBottomColor: '#d3d3d3', borderBottomWidth: 1, width: '40%', fontSize: 18 }} placeholder={'Amount'} />
          </View>
          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: '5%' }}>
            <FontAwesome name="calendar" size={24} color='#d3d3d3' />
            <TextInput placeholder={'Date'} style={{ borderBottomColor: '#d3d3d3', borderBottomWidth: 1, width: '40%', marginLeft: '4%', fontSize: 18 }} />
          </View>
        </View>
        <View style={{ marginLeft: '6%', marginTop: '6%', flexDirection: 'row' }}>
          <SimpleLineIcons name="note" size={22} color={'#d3d3d3'} />
          <TextInput placeholder={'Details of Item(s)'} style={{ borderBottomColor: '#d3d3d3', borderBottomWidth: 1, width: '40%', marginLeft: '2%', fontSize: 18, width: '70%' }} />
        </View>

        {/* Buttons */}
        <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
          <View style={styles.BtnView}>
          <TouchableOpacity style={styles.BtnStyle}>
            <Text style={{ fontSize: 20, color: 'white'}}>Delete Entry</Text>
          </TouchableOpacity>
          </View>
          <View style={styles.BtnView1}>
          <TouchableOpacity style={styles.BtnStyle}>
            <Text style={{ fontSize: 20, color: 'white'}}>Update Entry</Text>
          </TouchableOpacity>
          </View>
        </View>
      </View>
      </View>
      </SafeAreaView>
    </View>
);
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    width: '100%',
    height: '100%',
    marginTop: '7%'
  },

  headerWrap: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingHorizontal: 20,
    alignItems: 'center',
    height: '10%',
    borderBottomColor: 'orange',
    borderBottomWidth: 1,
  },

  headerText: {
    fontSize: 20,
    marginLeft: '4%',
  },

  searchWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: '8%',
    paddingHorizontal: '6%',
  },

  Search: {
    flex: 1,
    marginLeft: '3%',
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
  },

  SearchText: {
    fontSize: 18,
    marginBottom: '1%',
  },

  BtnView : {
    alignSelf: 'center',
    marginTop: '5%',
    marginLeft: '6%',
    backgroundColor: 'red',
    width: '35%',
    height: '35%',
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },

  BtnView1 : {
    alignSelf: 'center',
    marginTop: '5%',
    marginLeft: '6%',
    backgroundColor: 'green',
    width: '35%',
    height: '35%',
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },

  BtnStyle: {
    width: '100%',
    height: '100%',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
