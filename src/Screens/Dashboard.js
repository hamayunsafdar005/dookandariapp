import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, SafeAreaView, TextInput, TouchableOpacity, KeyboardAvoidingView } from 'react-native';
import { Ionicons, Feather, Entypo } from '@expo/vector-icons';

export default class Dashboard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      enterCode: false,
      spinner: false,
      country: {
        cca2: 'PK',
        callingCode: '92'
      }
    };
  }

    render() {
    return (
    <View style={styles.container}>
      <SafeAreaView>
        {/* header */}
        <View style={styles.headerWrap}>
          <Feather name="menu" size={24} color={'orange'} />
          <Text style={styles.headerText}>Kreactive Leo</Text>
        </View>
      </SafeAreaView>
      {/* SearchBar */}
      <View style={styles.searchWrap}>
        <Feather name="search" size={24} color={'grey'} />
        <View style={styles.Search}>
          <TextInput style={styles.SearchText} placeholder={"Search"} />
        </View>
      </View>

      {/* Flatlist */}
      {/* <View style={{ marginTop: '3%', alignSelf: 'center' }}>
        <Text style={{ fontSize: 20 }}>Grow your Business</Text>
      </View> */}

      
      <View style={{ alignSelf: 'center', alignItems: 'center', borderColor: 'black', borderWidth: 1, width: '80%', borderRadius: 25, marginTop: '5%', backgroundColor: '#d3d3d3 ' }}>
        <View style={{ backgroundColor: 'blue', alignSelf: 'center', width: '100%', alignItems: 'center', borderTopRightRadius: 25, borderTopLeftRadius: 25 }}>
          <Text style={{ fontSize: 22, color: 'white' }}>DOKAAN DAARI SUMMARY</Text>
        </View>
        <View style={{flexDirection: 'row', alignItems: 'center', marginTop: '3%'}}>
          <View>
            <Text style={{ fontSize: 17, color: 'green'}}>You Will Take</Text>
          </View>
          <View>
            <Entypo name="flow-line" size={50} color="blue" />
          </View>
          <View>
            <Text style={{ fontSize: 17, color: 'red'}}>You Will Get</Text>
          </View>
        </View>
      </View>

      {/* Add Button */}
      <View style={styles.BtnAdd}>
        <TouchableOpacity>
          <Ionicons name="person-add-sharp" size={48} color="orange" />
        </TouchableOpacity>
      </View>
    </View>
);
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    width: '100%',
    height: '100%',
    marginTop: '12%'
  },

  headerWrap: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    paddingHorizontal: 20,
  },

  headerText: {
    fontSize: 20,
    marginLeft: '4%',
  },

  searchWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: '5%',
    paddingHorizontal: '6%',
  },

  Search: {
    flex: 1,
    marginLeft: '3%',
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
  },

  SearchText: {
    fontSize: 18,
    marginBottom: '1%',
  },

  BtnAdd: {
    alignSelf: 'center',
    marginTop: '10%',
    alignSelf: 'flex-end',
    marginRight: '7%',
    position: 'relative',
  },

  BtnView : {
    alignSelf: 'center',
    marginTop: '2%',
    backgroundColor: 'blue',
    width: '70%',
    height: '7%',
    borderRadius: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },

  BtnStyle: {
    width: '100%',
    height: '100%',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
