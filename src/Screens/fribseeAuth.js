import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, SafeAreaView, TextInput, TouchableOpacity, Alert } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import Frisbee from 'frisbee';
import Spinner from 'react-native-loading-spinner-overlay';
import Form from 'react-native-form';
import CountryPicker from 'react-native-country-picker-modal';

const api = new Frisbee({
  baseURI: 'http://localhost:3000',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }
});

const MAX_LENGTH_CODE = 6;
const MAX_LENGTH_NUMBER = 20;

// if you want to customize the country picker
const countryPickerCustomStyles = {};

// your brand's theme primary color
const brandColor = '#744BAC';

export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      enterCode: false,
      spinner: false,
      country: {
        cca2: 'PK',
        callingCode: '92'
      }
    };
  }

  _getCode = () => {
    console.log("Get Code")
    this.setState({ spinner: true });

    setTimeout(async () => {

      try {

        const res = await api.post('/v1/verifications', {
          body: {
            ...this.refs.form.getValues(),
            ...this.state.country
          }
        });

        if (res.err) throw res.err;

        this.setState({
          spinner: false,
          enterCode: true,
          verification: res.body
        });
        this.refs.form.refs.textInput.setNativeProps({ text: '' });

        setTimeout(() => {
          Alert.alert('Sent!', "We've sent you a verification code", [{
            text: 'OK',
            onPress: () => this.refs.form.refs.textInput.focus()
          }]);
        }, 100);

      } catch (err) {
        // <https://github.com/niftylettuce/react-native-loading-spinner-overlay/issues/30#issuecomment-276845098>
        this.setState({ spinner: false });
        setTimeout(() => {
          Alert.alert('Oops!', err.message);
        }, 100);
      }

    }, 100);
  }

  _verifyCode = () => {

    this.setState({ spinner: true });

    setTimeout(async () => {

      try {

        const res = await api.put('/v1/verifications', {
          body: {
            ...this.refs.form.getValues(),
            ...this.state.country
          }
        });

        if (res.err) throw res.err;

        this.refs.form.refs.textInput.blur();
        // <https://github.com/niftylettuce/react-native-loading-spinner-overlay/issues/30#issuecomment-276845098>
        this.setState({ spinner: false });
        setTimeout(() => {
          Alert.alert('Success!', 'You have successfully verified your phone number');
        }, 100);

      } catch (err) {
        // <https://github.com/niftylettuce/react-native-loading-spinner-overlay/issues/30#issuecomment-276845098>
        this.setState({ spinner: false });
        setTimeout(() => {
          Alert.alert('Oops!', err.message);
        }, 100);
      }

    }, 100);

  }

  _onChangeText = (val) => {
    if (!this.state.enterCode) return;
    if (val.length === MAX_LENGTH_CODE)
    this._verifyCode();
  }

  _tryAgain = () => {
    this.refs.form.refs.textInput.setNativeProps({ text: '' })
    this.refs.form.refs.textInput.focus();
    this.setState({ enterCode: false });
  }

  _getSubmitAction = () => {
    this.state.enterCode ? this._verifyCode() : this._getCode();
  }

  _changeCountry = (country) => {
    this.setState({ country });
    this.refs.form.refs.textInput.focus();
  }

  _renderFooter = () => {

    if (this.state.enterCode)
      return (
        <View>
          <Text style={styles.wrongNumberText} onPress={this._tryAgain}>
            Enter the wrong number or need a new code?
          </Text>
        </View>
      );

      return (
        <View>
          <Text style={styles.disclaimerText}>By tapping "Send confirmation code" above, we will send you an SMS to confirm your phone number. Message &amp; data rates may apply.</Text>
        </View>
      );
    }

    _renderCountryPicker = () => {

      if (this.state.enterCode)
        return (
          <View />
        );
  
      return (
        <CountryPicker
          ref={'countryPicker'}
          closeable
          style={styles.countryPicker}
          onChange={this._changeCountry}
          cca2={this.state.country.cca2}
          styles={countryPickerCustomStyles}
          translation='eng'/>
      );
  
    }
  
    _renderCallingCode = () => {
  
      if (this.state.enterCode)
        return (
          <View />
        );
  
      return (
        <View style={styles.callingCodeView}>
          <Text style={styles.callingCodeText}>+{this.state.country.callingCode}</Text>
        </View>
      );
  
    }

    render() {

      let headerText = `What's your ${this.state.enterCode ? 'verification code' : 'phone number'}?`
      let buttonText = this.state.enterCode ? 'Verify confirmation code' : 'Send confirmation code';
      let textStyle = this.state.enterCode ? {
        height: 50,
        textAlign: 'center',
        fontSize: 40,
        fontWeight: 'bold',
        fontFamily: 'Courier'
      } : {};

  return (
    // <View style={styles.container}>
    // <SafeAreaView style={{ width: '100%', height: '100%', marginTop: '25%'}}>
    //   <View style={{ alignSelf: 'flex-start', height: '15%', width: '100%', marginLeft: '5%'}}>
    //     <Ionicons name="arrow-back-circle-sharp" size={40} color="orange" />
    //   </View>
    //   <View style={{ marginLeft: '20%', width: '80%'}}>
    //     <Text style={{ fontSize: 25, fontWeight: 'bold'}}>OTP Verification Code</Text>
    //     <Text style={{ marginTop: '2%', fontSize: 18}}>An automated 4 digit verification</Text>
    //     <Text style={{ fontSize: 18}}>code will be received shortly to your mobile number through sms</Text>
    //   </View>
    //   <View style={{ alignSelf: 'center', alignItems: 'baseline', marginTop: '5%'}}>
    //   </View>
    //   <View style={{ alignSelf: 'center', marginTop: '25%', backgroundColor: 'blue', width: '70%', height: '7%', borderRadius: 25, alignItems: 'center', justifyContent: 'center'}}>
    //     <TouchableOpacity style={{ width: '100%', height: '100%', alignSelf: 'center', alignItems: 'center', justifyContent: 'center'}}>
    //       <Text style={{ fontSize: 20, color: 'white'}}>Next</Text>
    //     </TouchableOpacity>
    //   </View>
    //   <View style={{ alignSelf: 'center', marginTop: '3%',}}>
    //     <TouchableOpacity>
    //       <Text style={{ fontSize: 20, color: 'grey'}}>Cancel</Text>
    //     </TouchableOpacity>
    //   </View>
    //   <StatusBar style="auto" />
    //   </SafeAreaView>
    // </View>

    <View style={styles.container}>

          <View style={{ marginLeft: '12%', width: '100%', marginTop: '12%'}}>
            <Ionicons name="arrow-back-circle-sharp" size={40} color="orange" />
          </View>

        <Text style={styles.header}>{headerText}</Text>

        <Form ref={'form'} style={styles.form}>

          <View style={{ flexDirection: 'row', marginLeft: '10%' }}>

            {/* {this._renderCountryPicker()} */}
            {this._renderCallingCode()}

            <TextInput
              ref={'textInput'}
              name={this.state.enterCode ? 'code' : 'phoneNumber' }
              type={'TextInput'}
              underlineColorAndroid={'transparent'}
              autoCapitalize={'none'}
              autoCorrect={false}
              onChangeText={this._onChangeText}
              placeholder={this.state.enterCode ? '_ _ _ _ _ _' : 'Phone Number'}
              keyboardType={Platform.OS === 'ios' ? 'number-pad' : 'numeric'}
              style={[ styles.textInput, textStyle ]}
              returnKeyType='go'
              autoFocus
              placeholderTextColor={brandColor}
              selectionColor={brandColor}
              maxLength={this.state.enterCode ? 6 : 10}
              onSubmitEditing={this._getSubmitAction} />

          </View>

          <TouchableOpacity style={styles.button} onPress={this._getSubmitAction}>
            <Text style={styles.buttonText}>{ buttonText }</Text>
          </TouchableOpacity>

          {this._renderFooter()}

        </Form>

        <Spinner
          visible={this.state.spinner}
          textContent={'One moment...'}
          textStyle={{ color: '#fff' }} />

      </View>
  );
}
}

const styles = StyleSheet.create({
  // container: {
  //   flex: 1,
  //   backgroundColor: '#fff',
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   width: '100%',
  //   height: '100%',
  // },

  // underlineStyleBase: {
  //   width: 30,
  //   height: 45,
  //   borderWidth: 0,
  //   borderBottomWidth: 1,
  // },
 
  // underlineStyleHighLighted: {
  //   borderColor: "#03DAC6",
  // },

  countryPicker: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  container: {
    flex: 1,
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  header: {
    textAlign: 'center',
    marginTop: 60,
    fontSize: 22,
    margin: 20,
    color: '#4A4A4A',
  },
  form: {
    margin: 20
  },
  textInput: {
    padding: 0,
    margin: 0,
    flex: 1,
    fontSize: 20,
    color: brandColor
  },
  button: {
    marginTop: 20,
    height: 50,
    backgroundColor: brandColor,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  buttonText: {
    color: '#fff',
    // fontFamily: 'Helvetica',
    fontSize: 16,
    fontWeight: 'bold'
  },
  wrongNumberText: {
    margin: 10,
    fontSize: 14,
    textAlign: 'center'
  },
  disclaimerText: {
    marginTop: 30,
    fontSize: 12,
    color: 'grey'
  },
  callingCodeView: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  callingCodeText: {
    fontSize: 20,
    color: brandColor,
    // fontFamily: 'Helvetica',
    fontWeight: 'bold',
    paddingRight: 10
  }
});
